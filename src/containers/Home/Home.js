import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {loadDelivery} from 'redux/modules/tracking';
import {Tracking} from 'Components';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      origin: null,
      destination: null,
      directions: null,
    };
  }

  componentWillMount() {
    this.loadDelivery();
  }

  loadDelivery = () => {
    const delivery = this.props.params.delivery || null;

    this.props.dispatch(loadDelivery(delivery));
  }


  render() {
    const {
      tracking: {
        courierLocation,
        delivery,
        company,
        courier,
        loading,
        loaded,
      }
    } = this.props;

    const style = require('./Home.scss');

    const trackingLoaded = !loading && loaded;

    const footer = (
      <div className={`col-xs-12 col-md-12 text-center ${delivery ? null : style.logoFooter}`}>
        <span className={style.poweredBy}>Powered By</span> <img className={style.sherpafleetlogo} src="/sf-full-logo.png" width="115" alt="SherpaFleet Logo" />
      </div>
    );

    return (
      <div className={`container-fluid ${style.wrapper}`}>
        <Helmet title="Delivery Tracking"/>
        {delivery && trackingLoaded && <Tracking courierLocation={courierLocation} company={company} delivery={delivery} courier={courier} style={style} footer={footer} />}
        {delivery === undefined && trackingLoaded && <div className={style.notFound}> Delivery not found</div>}
        {delivery === undefined && trackingLoaded && footer}
      </div>
    );
  }
}

Home.propTypes = {
  dispatch: PropTypes.func.isRequired,
  tracking: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  tracking: state.tracking,
  location: ownProps.location,
});

export default connect(mapStateToProps)(Home);
