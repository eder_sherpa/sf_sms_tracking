import React, { Component, PropTypes } from 'react';
import { withGoogleMap, GoogleMap, Marker, DirectionsRenderer } from 'react-google-maps';
import moment from 'moment';
import _ from 'underscore';

const TrackingMap = withGoogleMap(props => {
  return (
    <GoogleMap {...props}>
      {props.directions && <DirectionsRenderer directions={props.directions} />}
      {_.map(props.markers, (marker, key) => {
        return (
          <Marker {...marker} key={key} />
        );
      })}
    </GoogleMap>
  );
});

class Tracking extends Component {
  constructor(props) {
    super(props);

    this.state = {
      origin: null,
      destination: null,
      directions: null,
      defaultCenter: {
        lat: -33.847973,
        lng: 150.6517728
      },
      markers: [],
    };
  }

  componentWillMount() {
    this.getDirections();
  }

  componentDidMount() {
    this.createMapMarkers();
  }

  getDirections = () => {
    const { delivery } = this.props;

    /* eslint-disable */
    const DirectionsService = new google.maps.DirectionsService();

    DirectionsService.route({
      origin: delivery.pickup_address.location,
      destination: delivery.delivery_address.location,
      travelMode: google.maps.TravelMode.DRIVING,
    }, (result, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        this.setState({
          directions: result,
        });
      } else {
        console.error(`error fetching directions ${result}`);
      }
    });
    /* eslint-enable */
  }

  createCourierMarker = (courierLocation) => {
    const icon = 'M18.92 6.01C18.72 5.42 18.16 5 17.5 5h-11c-.66 0-1.21.42-1.42 1.01L3 12v8c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-1h12v1c0 .55.45 1 1 1h1c.55 0 1-.45 1-1v-8l-2.08-5.99zM6.5 16c-.83 0-1.5-.67-1.5-1.5S5.67 13 6.5 13s1.5.67 1.5 1.5S7.33 16 6.5 16zm11 0c-.83 0-1.5-.67-1.5-1.5s.67-1.5 1.5-1.5 1.5.67 1.5 1.5-.67 1.5-1.5 1.5zM5 11l1.5-4.5h11L19 11H5z';

    const courierMarker = {
      position: courierLocation,
      icon: {
        path: icon,
        fillColor: '#0097a7',
        fillOpacity: 0.8,
        scale: 1.5,
        strokeColor: '#536e7b',
        strokeWeight: 1
      }
    };

    return courierMarker;
  }

  createMapMarkers = () => {
    const {courierLocation} = this.props;

    const markers = [];

    const courierMarker = this.createCourierMarker(courierLocation);

    markers.push(courierMarker);

    this.setState({
      markers: markers
    });
  }

  render() {
    const {
      defaultCenter,
      markers,
      directions,
    } = this.state;

    const {
      courierLocation,
      delivery,
      courier,
      company,
      style,
      footer,
    } = this.props;

    const mapWrapper = <div style={{ height: '300px' }} />;

    const _defaultCenter = courierLocation ? courierLocation : defaultCenter;

    const companyLogo = company.logo ? company.logo : '/sherpaFleet_logo.png';

    let courierPhone = null;

    if (!_.isNull(courier.phone)) {
      courierPhone = `${courier.phone.country_code}${courier.phone.number}`;
    }

    return (
      <div className={`row ${style.row}`}>
        <div className={`col-xs-12 col-md-5 col-md-offset-1 ${style.infoDesktopAlign}`}>
          <div className={`company-info ${style.companyInfo}`}>
            <div className={`col-xs-2 col-md-12 text-center ${style.displayBlock} ${style.paddingReset}`}>
              <img width="70" className={style.companyLogo} src={companyLogo} alt="company-logo" />
            </div>
            <div className={`col-xs-10 col-md-12 ${style.inlineBlock}`} style={{ paddingLeft: '20px' }}>
              <small className={`hidden-md hidden-lg ${style.displayBlock}`} style={{ padding: 0 }}>You have a delivery on the way from</small>
              <span className={style.companyName}>{company.name}</span>
            </div>
            <div className="col-xs-12 hidden-xs hidden-sm">
              <div className={style.text}>{'Your delivery is on it\'s way!'}</div>
              <div className={style.jobInfo}>
                <span className={style.label}>Your order:</span>
                <span className={style.description}>{delivery.item_details}</span>
              </div>
            </div>
          </div>
        </div>

        <div className="col-xs-12 col-md-5" style={{ padding: 0 }}>
          <div className={`eta ${style.eta}`}>
            <span className={style.label}>ETA</span>
            <span className={style.time}>{moment(delivery.eta).format('dddd, HH:mm a')}</span>
          </div>
          <div className="custom-map" style={{ height: '300px', backgroundColor: '#eff0f1' }}>
            <TrackingMap containerElement={mapWrapper} mapElement={mapWrapper} defaultCenter={_defaultCenter} markers={markers} directions={directions} />
          </div>
        </div>

        <div className={`col-xs-12 col-md-5 col-md-offset-6 ${style.courierDesktopAlign}`}>
          <div className={`courier-info ${style.courierInfo}`}>
            <div className={style.courierPhoto}>
              {courier.photo ? <img width="50" className={style.img} src={courier.photo} alt="driver profile" /> : <i className={`material-icons ${style.courierAvatar}`}>account_circle</i>}
            </div>
            <div className={style.courierName}>
              <span className={style.label}>Your Courier</span>
              <span className={style.name} title={`${courier.first_name} ${courier.last_name}`}>{courier.first_name} {courier.last_name}</span>
            </div>
            <div className={`hidden-md hidden-lg ${style.callCourierBtn} ${courierPhone === null ? 'hidden' : ''}`}>
              <a href={!_.isNull(courierPhone) ? `tel:+${courierPhone}` : ''} className={`btn ${style.btn}`}><span className={`material-icons ${style.callCourierBtn.icon}`}>phone</span></a>
            </div>
          </div>
        </div>

        <div className="col-xs-12 hidden-md hidden-lg">
          <div className={`job-info ${style.jobInfo}`}>
            <span className={style.label}>Your order:</span>
            <span className={style.description}>{delivery.item_details}</span>
          </div>
        </div>
        {footer}
      </div>
    );
  }
}

Tracking.propTypes = {
  courierLocation: PropTypes.object.isRequired,
  delivery: PropTypes.object.isRequired,
  company: PropTypes.object.isRequired,
  courier: PropTypes.object.isRequired,
  style: PropTypes.object.isRequired,
  footer: PropTypes.element.isRequired,
};

export default Tracking;
