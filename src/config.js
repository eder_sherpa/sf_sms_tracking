require('babel-polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT,
  app: {
    title: 'SherpaFleet',
    description: 'SMS Delivery Tracking.',
    head: {
      titleTemplate: 'SherpaFleet - SMS Delivery Tracking.',
      meta: [
        {name: 'description', content: 'Sherpa delivery management platform.'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'Sherpa SaaS Client'},
        {property: 'og:image', content: 'https://react-redux.herokuapp.com/logo.jpg'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'Sherpa SaaS Client'},
        {property: 'og:description', content: 'Sherpa delivery management platform.'},
        {property: 'og:card', content: 'summary'},
        {property: 'og:site', content: 'sherpa.net.au'},
        {property: 'og:creator', content: '@edercampelo'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    }
  },

}, environment);
