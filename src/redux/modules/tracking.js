const LOAD = '@deliveries/LOAD';
const LOAD_SUCCESS = '@deliveries/LOAD_SUCCESS';
const LOAD_FAIL = '@deliveries/LOAD_FAIL';
export const UPDATE_LOCATION = '@deliveries/UPDATE_LOCATION';

const initialState = {
  loaded: false,
  loading: true,
  error: null,
  location: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loaded: false,
        loading: true,
        data: null,
        error: null,
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        error: null,
        courierLocation: action.result.data.courier.location,
        ...action.result.data,
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: true,
        delivery: undefined,
        error: action.error
      };
    case UPDATE_LOCATION:
      return {
        ...state,
        courierLocation: action.data,
      };
    default:
      return state;
  }
}

export function loadDelivery(delivery) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get(`https://deliveries.${__STACK__}.sherpafleet.com/api/v1/deliveries/${delivery}/tracking`)
  };
}
