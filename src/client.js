/**
 * THIS IS THE ENTRY POINT FOR THE CLIENT, JUST LIKE server.js IS THE ENTRY POINT FOR THE SERVER.
 */
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
import {Provider} from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { ReduxAsyncConnect } from 'redux-async-connect';
import useScroll from 'scroll-behavior/lib/useStandardScroll';
import {UPDATE_LOCATION} from 'redux/modules/tracking';
import WSInstance from 'utils/rtmWS';

import getRoutes from './routes';

const client = new ApiClient();
const _browserHistory = useScroll(() => browserHistory)();
const dest = document.getElementById('content');
const store = createStore(_browserHistory, client, window.__data);
const history = syncHistoryWithStore(_browserHistory, store);

const component = (
  <Router render={(props) => <ReduxAsyncConnect {...props} helpers={{client}} filter={item => !item.deferred} />} history={history}>
    {getRoutes(store)}
  </Router>
);

ReactDOM.render(
  <Provider store={store} key="provider">
    {component}
  </Provider>,
  dest
);

if (process.env.NODE_ENV !== 'production') {
  window.React = React; // enable debugger

  if (!dest || !dest.firstChild || !dest.firstChild.attributes || !dest.firstChild.attributes['data-react-checksum']) {
    console.error('Server-side React render was discarded. Make sure that your initial render does not contain any client-side code.');
  }
}

const delivery = window.location.pathname.replace('/', '') || null;

const sock = {
  ws: null,
  URL: `wss://rtm.${__STACK__}.sherpafleet.com/rtm/channels/${delivery}/`,
  lastId: '',
  wsDipatcher: (msg) => {
    if (msg.id !== sock.lastId) {
      sock.lastId = msg.id;
      switch (msg.type) {
        case 'user_location_updated':
          return store.dispatch({type: UPDATE_LOCATION, data: msg});
        default:
          return null;
      }
    }
  },
  startWS: () => {
    if (delivery) {
      sock.ws = new WSInstance(sock.URL, delivery, sock.wsDipatcher);
    }
  },
  wsListener: () => {
    try {
      sock.startWS();
    } catch (err) {
      console.error(`Socket Error: ${err}`);
    }
  },
  ping: () => {
    setTimeout(sock.ping, 15000);

    try {
      sock.ws.send({type: 'ping'});
    } catch (err) {
      console.log(`Socket Error: ${err}`);
    }
  },
};

store.subscribe(() => sock.wsListener());
