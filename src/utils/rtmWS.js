import _ from 'underscore';

export default class RtmWS {
  constructor(url, token, dispatcher) {
    this.websocket = new WebSocket(url);

    this.websocket.onmessage = (event) => {
      _.defer(dispatcher, JSON.parse(event.data));
    };
  }

  send(msg) {
    if (this.websocket && this.websocket.readyState === 1) {
      this.websocket.send(JSON.stringify(msg));
    } else {
      throw new Error('WebSocket is not in OPEN state.');
    }
  }

  close() {
    this.websocket.close();
  }
}
